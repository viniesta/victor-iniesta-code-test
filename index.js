/**
 * Initializes the TODO server, connecting
 * first to the database and in case of success
 * launching the HTTP server
 */
var winston = require('winston');
var Server = require('./lib/server');
var Database = require('./lib/database');

var todo_db = new Database();
todo_db.connect()
.then(function() {
	var todo_server = new Server(todo_db);
	todo_server.start();
},
function() {
	winston.error('Error in database - Could not start server');
})
.fail(function(err) {
	winston.error('Unhandled error', err);	
});
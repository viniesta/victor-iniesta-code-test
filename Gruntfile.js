/*global module:false*/
module.exports = function(grunt) {
    // Project configuration.
    grunt.initConfig({
        // Task configuration.
        jshint: {
            options: {
                curly: true,
                eqeqeq: true,
                immed: true,
                latedef: true,
                newcap: true,
                noarg: true,
                sub: true,
                undef: true,
                unused: true,
                boss: true,
                eqnull: true,
                globals: {
                    describe: true,
                    before : true,
                    after : true,
                    it : true,
                    beforeEach : true,
                    afterEach : true
                },
                node: true,
                strict: false
            },
            gruntfile: {
                src: 'Gruntfile.js'
            },
            lib_test: {
                src: ['index.js', 'lib/**/*.js', 'test/**/*.js']
            }
        },
        simplemocha: {
            options: {
                timeout: 20000,
                ignoreLeaks: false,
                ui: 'bdd',
            },
            dev: {
                options: {
                    reporter: 'spec'
                },
                src: [                    
                    'test/**/*.js'                    
                ]
            }
        }
    });

    // These plugins provide necessary tasks.
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-simple-mocha');

    // Default task.
    grunt.registerTask('default', ['jshint', 'simplemocha:dev']);
};

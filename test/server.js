var mockery = require('mockery');
var should = require('should');
var request = require('request');
var config = require('./mock_config');
var db;
var todo_server;
var base_url;

describe('TODO server', function() {
    before(function(done){
        mockery.enable({
            warnOnReplace: true,
            warnOnUnregistered: false,
            useCleanCache: true
        });

        base_url = 'http://127.0.0.1:' + config.server.port;
        mockery.registerSubstitute('../config',
                                __dirname + '/mock_config');
        var Database = require('../lib/database');
        var Server = require('../lib/server');
        db = new Database();
        db.connect()
        .then(function() {
            todo_server = new Server(db);
            todo_server.simulate_bad_session(false);
            todo_server.start();
            return done();
        }, 
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('POST request to /todo with headers and body', function(done) {
        var options = {
            method : 'POST',
            url : base_url + '/todo',
            headers : {
                'Content-Type' : 'application/json',
                'userId': 'user',
                'sessionId' : 'sessionId'                
            },
            body : JSON.stringify({"title":"Note title","body":"Note body"})
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(200);            
            done();
        });
    });

    it('POST request to /todo without session headers and body', function(done) {
        var options = {
            method : 'POST',
            url : base_url + '/todo',
            headers : {
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify({"title":"Note title","body":"Note body"})
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(401);            
            done();
        });
    });

    it('POST request to /todo with headers and no body', function(done) {
        var options = {
            method : 'POST',
            url : base_url + '/todo',
            headers : {
                'Content-Type' : 'application/json',
                'userId': 'user',
                'sessionId' : 'sessionId'                
            }
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(400);            
            done();
        });
    });

    it('POST request to /todo without session headers and no body', function(done) {
        var options = {
            method : 'POST',
            url : base_url + '/todo',
            headers : {
                'Content-Type' : 'application/json'
            }
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(401);            
            done();
        });
    });

    it('POST request to /todo with session headers body, but not Content-Type header', function(done) {
        var options = {
            method : 'POST',
            url : base_url + '/todo',
            headers : {
                'userId': 'user',
                'sessionId' : 'sessionId'                
            },
            body : JSON.stringify({"title":"Note title","body":"Note body"})
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(400);
            done();
        });
    });

    it('POST request to /todo specifying note ID', function(done) {
        var ObjectId = require('mongoose').Types.ObjectId; 
        var random_id = new ObjectId();

        var options = {
            method : 'POST',
            url : base_url + '/todo/' + random_id.toHexString(),
            headers : {
                'Content-Type' : 'application/json',
                'userId': 'user',
                'sessionId' : 'sessionId'                
            },
            body : JSON.stringify({"title":"Note title","body":"Note body"})
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(404);
            done();
        });
    });

    it('POST request to /todo with wrong body data', function(done) {
        var options = {
            method : 'POST',
            url : base_url + '/todo',
            headers : {
                'Content-Type' : 'application/json',
                'userId': 'user',
                'sessionId' : 'sessionId'                
            },
            body : JSON.stringify({"user":"Blabla","id":22})
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(400);
            done();
        });
    });

    it('PUT request to /todo with headers and body', function(done) {
        db.add_note('user', 'Note title', 'Note body')
        .then(function(note) {
            var options = {
                method : 'PUT',
                url : base_url + '/todo/' + note._id.toHexString(),
                headers : {
                    'Content-Type' : 'application/json',
                    'userId': 'user',
                    'sessionId' : 'sessionId'                
                },
                body : JSON.stringify({
                    "title":"New Note title",
                    "body":"New Note body"
                })
            };

            request(options, function(err, res) {
                should.not.exist(err);            
                res.statusCode.should.equal(200);            
                done();
            });
        },
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('PUT request to /todo with headers and body but no note ID', function(done) {        
        var options = {
            method : 'PUT',
            url : base_url + '/todo',
            headers : {
                'Content-Type' : 'application/json',
                'userId': 'user',
                'sessionId' : 'sessionId'
            },
            body : JSON.stringify({
                "title":"New Note title",
                "body":"New Note body"
            })
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(404);
            done();
        });        
    });

    it('PUT request to /todo with no authentication headers and body', function(done) {
        db.add_note('user', 'Note title', 'Note body')
        .then(function(note) {
            var options = {
                method : 'PUT',
                url : base_url + '/todo/' + note._id.toHexString(),
                headers : {
                    'Content-Type' : 'application/json'           
                },
                body : JSON.stringify({
                    "title":"New Note title",
                    "body":"New Note body"
                })
            };

            request(options, function(err, res) {
                should.not.exist(err);            
                res.statusCode.should.equal(401);            
                done();
            });
        },
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('PUT request to /todo with no Content-Type headers and body', function(done) {
        db.add_note('user', 'Note title', 'Note body')
        .then(function(note) {
            var options = {
                method : 'PUT',
                url : base_url + '/todo/' + note._id.toHexString(),
                headers : {
                    'userId': 'user',
                    'sessionId' : 'sessionId'                
                },
                body : JSON.stringify({
                    "title":"New Note title",
                    "body":"New Note body"
                })
            };

            request(options, function(err, res) {
                should.not.exist(err);            
                res.statusCode.should.equal(400);
                done();
            });
        },
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('PUT request to /todo with headers and no body', function(done) {
        db.add_note('user', 'Note title', 'Note body')
        .then(function(note) {
            var options = {
                method : 'PUT',
                url : base_url + '/todo/' + note._id.toHexString(),
                headers : {
                    'Content-Type' : 'application/json',
                    'userId': 'user',
                    'sessionId' : 'sessionId'                
                }
            };

            request(options, function(err, res) {
                should.not.exist(err);            
                res.statusCode.should.equal(400);
                done();
            });
        },
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('PUT request to /todo with headers and wrong body', function(done) {
        db.add_note('user', 'Note title', 'Note body')
        .then(function(note) {
            var options = {
                method : 'PUT',
                url : base_url + '/todo/' + note._id.toHexString(),
                headers : {
                    'Content-Type' : 'application/json',
                    'userId': 'user',
                    'sessionId' : 'sessionId'                
                },
                body : JSON.stringify({
                    "bad_param_1":"New Note title",
                    "bad_param_2":"New Note body"
                })
            };

            request(options, function(err, res) {
                should.not.exist(err);            
                res.statusCode.should.equal(400);
                done();
            });
        },
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('PUT request to /todo with headers and different user', function(done) {
        db.add_note('user', 'Note title', 'Note body')
        .then(function(note) {
            var options = {
                method : 'PUT',
                url : base_url + '/todo/' + note._id.toHexString(),
                headers : {
                    'Content-Type' : 'application/json',
                    'userId': 'other user',
                    'sessionId' : 'sessionId'                
                },
                body : JSON.stringify({
                    "title":"New Note title",
                    "body":"New Note body"
                })
            };

            request(options, function(err, res) {
                should.not.exist(err);            
                res.statusCode.should.equal(404);
                done();
            });
        },
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('PUT request to /todo and wrong note ID', function(done) {
        var ObjectId = require('mongoose').Types.ObjectId; 
        var random_id = new ObjectId();
        var options = {
            method : 'PUT',
            url : base_url + '/todo/' + random_id.toHexString(),
            headers : {
                'Content-Type' : 'application/json',
                'userId': 'user',
                'sessionId' : 'sessionId'                
            },
            body : JSON.stringify({
                "title":"New Note title",
                "body":"New Note body"
            })
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(404);
            done();
        });        
    });

    it('DELETE request to /todo with headers', function(done) {
        db.add_note('user', 'Note title', 'Note body')
        .then(function(note) {
            var options = {
                method : 'DELETE',
                url : base_url + '/todo/' + note._id.toHexString(),
                headers : {
                    'userId': 'user',
                    'sessionId' : 'sessionId'                
                }
            };

            request(options, function(err, res) {
                should.not.exist(err);            
                res.statusCode.should.equal(200);            
                done();
            });
        },
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('DELETE request to /todo with headers but no note ID', function(done) {        
        var options = {
            method : 'DELETE',
            url : base_url + '/todo',
            headers : {
                'userId': 'user',
                'sessionId' : 'sessionId'
            }
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(404);
            done();
        });        
    });

    it('DELETE request to /todo with no authentication headers', function(done) {
        db.add_note('user', 'Note title', 'Note body')
        .then(function(note) {
            var options = {
                method : 'DELETE',
                url : base_url + '/todo/' + note._id.toHexString()
            };

            request(options, function(err, res) {
                should.not.exist(err);            
                res.statusCode.should.equal(401);            
                done();
            });
        },
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('DELETE request to /todo with headers and wrong user', function(done) {
        db.add_note('user', 'Note title', 'Note body')
        .then(function(note) {
            var options = {
                method : 'DELETE',
                url : base_url + '/todo/' + note._id.toHexString(),
                headers : {
                    'userId': 'other user',
                    'sessionId' : 'sessionId'                
                }
            };

            request(options, function(err, res) {
                should.not.exist(err);            
                res.statusCode.should.equal(404);
                done();
            });
        },
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('DELETE request to /todo and wrong note ID', function(done) {
        var ObjectId = require('mongoose').Types.ObjectId; 
        var random_id = new ObjectId();
        var options = {
            method : 'DELETE',
            url : base_url + '/todo/' + random_id.toHexString(),
            headers : {                
                'userId': 'user',
                'sessionId' : 'sessionId'                
            }
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(404);
            done();
        });        
    });

    it('GET request to /todo', function(done) {
        var ObjectId = require('mongoose').Types.ObjectId; 
        var random_id = new ObjectId();
        var options = {
            method : 'GET',
            url : base_url + '/todo/' + random_id.toHexString(),
            headers : {
                'Content-Type' : 'application/json',
                'userId': 'user',
                'sessionId' : 'sessionId'                
            }
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(404);
            done();
        });
    });

    it('GET request to /user with stored notes', function(done) {
        db.add_note('user', 'Note title', 'Note body')
        .then(function(note) {
            var options = {
                method : 'GET',
                url : base_url + '/user/user'
            };

            request(options, function(err, res) {
                should.not.exist(err);            
                res.statusCode.should.equal(200);
                should.exist(res.body);
                var note_array = JSON.parse(res.body);
                var is_note = note_array.some(function(user_note){
                    return (user_note._id === note._id.toHexString());
                });

                return done((is_note) ? null : new Error('Note not in the list'));
            });
        },
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('GET request to /user with no stored notes', function(done) {        
        var options = {
            method : 'GET',
            url : base_url + '/user/other_user'
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(200);
            should.exist(res.body);
            var note_array = JSON.parse(res.body);
            note_array.length.should.equal(0);
            return done();
        });
    });

    it('POST request to /user', function(done) {
        var options = {
            method : 'POST',
            url : base_url + '/user/test_user'
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(404);
            done();
        });
    });

    it('PUT request to /user', function(done) {
        var options = {
            method : 'PUT',
            url : base_url + '/user/test_user'
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(404);
            done();
        });
    });

    it('DELETE request to /user', function(done) {
        var options = {
            method : 'DELETE',
            url : base_url + '/user/test_user'
        };

        request(options, function(err, res) {
            should.not.exist(err);            
            res.statusCode.should.equal(404);
            done();
        });
    });

    after(function(done) {
        db.delete_all_documents().then(function() {
            mockery.deregisterSubstitute(__dirname + '/config');
            mockery.resetCache();
            mockery.disable();
            done();
        });        
    });
});
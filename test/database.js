var mockery = require('mockery');
var should = require('should');
var db;

describe('Moongose database', function() {
    before(function(done){
        mockery.enable({
            warnOnReplace: true,
            warnOnUnregistered: false,
            useCleanCache: true
        });

        mockery.registerSubstitute('../config',
                                __dirname + '/mock_config');
        var Database = require('../lib/database');
        db = new Database();
        db.connect().then(done);
    });

    it('Adding a new note', function(done){
        db.add_note('Test user', 
                    'Test title',
                    'Test Body')
        .then(function(note) {
            should.exist(note);
            return done();
        }, 
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('Modifying an existing note', function(done){
        db.add_note('Test user', 
                    'Test title',
                    'Test Body')
        .then(function(note) {            
            should.exist(note);
            return note;
        })
        .then(function(note) {            
            return db.update_note('Test user',
                            note._id,
                            'New test title',
                            'New test body')
            .then(function(updated_entries) {                
                updated_entries.should.equal(true);
                return done();
            });
        }, 
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('Modifying an unexisting note',function(done){
        var ObjectId = require('mongoose').Types.ObjectId; 
        var bad_id = new ObjectId();

        db.update_note(
                    'Test user',
                    bad_id,
                    'New test title',
                    'New test body')
        .then(function(updated_entries) {                
            updated_entries.should.equal(false);
            return done();
        }, 
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('Modifying an existing note with a different user', function(done) {
        db.add_note('Test user', 
                    'Test title',
                    'Test Body')
        .then(function(note) {            
            should.exist(note);
            return note;
        })
        .then(function(note) {            
            return db.update_note('Other user',
                            note._id,
                            'New test title',
                            'New test body')
            .then(function(updated_entries) {                
                updated_entries.should.equal(false);
                return done();
            });
        }, 
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('Removing an existing note', function(done){
        db.add_note('Test user', 
                    'Test title',
                    'Test Body')
        .then(function(note) {            
            should.exist(note);
            return note;
        })
        .then(function(note) {            
            return db.remove_note('Test user',
                                    note._id)
            .then(function(removed_note) {
                should.exist(removed_note);                
                removed_note._id.toHexString().should.equal(note._id.toHexString()); 
                return done();
            });
        }, 
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('Removing an unexisting note', function(done){
        var ObjectId = require('mongoose').Types.ObjectId; 
        var bad_id = new ObjectId();

        db.remove_note(
                    'Test user',
                    bad_id)
        .then(function(note) {
            should.not.exist(note);
            return done();
        }, 
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('Removing an existing note with a different user', function(done) {
        db.add_note('Test user', 
                    'Test title',
                    'Test Body')
        .then(function(note) {            
            should.exist(note);
            return note;
        })
        .then(function(note) {            
            return db.remove_note('Other user',
                            note._id)
            .then(function(removed_note) {
                should.not.exist(removed_note);
                return done();
            });
        }, 
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('Removing an existing note twice', function(done){
        db.add_note('Test user', 
                    'Test title',
                    'Test Body')
        .then(function(note) {            
            should.exist(note);
            return note;
        })
        .then(function(note) {            
            return db.remove_note('Test user',
                                    note._id)
            .then(function(removed_note) {
                should.exist(removed_note);                
                removed_note._id.toHexString().should.equal(note._id.toHexString()); 
                return note;
            })
            .then(function(note) {            
                return db.remove_note('Test user',
                                        note._id)
                .then(function(removed_note) {
                    should.not.exist(removed_note);                    
                    return done();
                });
            });
        }, 
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('Getting notes for a user with notes', function(done){
        db.add_note('Test user', 
                    'Test title',
                    'Test Body')
        .then(function(note) {            
            should.exist(note);
            return note;
        })
        .then(function(note) {            
            return db.get_user_notes('Test user')
            .then(function(notes) {
                should.exist(notes);
                /* Check the added note is in the returned note array */
                var is_note = notes.some(function(user_note){
                    return (user_note._id.toHexString() === note._id.toHexString());
                });

                return done((is_note) ? null : new Error('Note not in the list'));
            });
        }, 
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('Getting notes for a user with no notes', function(done){
        db.add_note('New Test user', 
                    'Test title',
                    'Test Body')
        .then(function(note) {            
            should.exist(note);
            return note;
        })
        .then(function(note) {            
            return db.remove_note('New Test user',
                                    note._id)
            .then(function(removed_note) {
                should.exist(removed_note);                
                removed_note._id.toHexString().should.equal(note._id.toHexString()); 
                return;
            })
            .then(function() {            
                return db.get_user_notes('New Test user')
                .then(function(notes) {
                    notes.length.should.equal(0);
                    return done();
                });
            });
        }, 
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    it('Getting notes for an unexisting user', function(done){
        db.get_user_notes('Other Test user')
        .then(function(notes) {
            notes.length.should.equal(0);
            return done();
        }, 
        function(err) {
            return done(err);
        })
        .fail(function(err) {
            return done(err);
        });
    });

    after(function(done) {
        db.delete_all_documents().then(function(){
            mockery.deregisterSubstitute(__dirname + '/config');
            mockery.resetCache();
            mockery.disable();
            done();
        });        
    });
});
/**
 * HTTP server that accept client requests
 * to manage user TODO notes
 */
var winston = require('winston');
var express = require('express');
var config = require('../config');

/**
 * Server class constructor
 * Initializes and configures the HTTP server
 */
function Server(db) {
	var self = this;
	this.app = express();

	/**
	 * Controls if the simulated session will reject
	 * some of the requests doe
	 */
	this.sim_bad_session = true;
	this.counter = 0; /* Keeps count of the number of requests done */

	/* Server configuration */
	this.app.set('env', 'development');
	this.app.configure('development', function() {
		self.app.use(express.errorHandler(
			{
				dumpExceptions:true,
				showStack:true 
			})
		);
	});

	this.app.enable('strict routing');	
	this.app.use(express.json());
	this.app.use(express.urlencoded());
	this.app.use(express.cookieParser());
	this.app.use(express.session({
		secret: 'my-little-secret'
	}));

	/**
	 * Session-simulation middleware
	 * If enabled, rejects 1 of 10 requests
	 * @returns True if the session is accepted, false otherwise
	 */
	this.app.use(function(req, res, next) {
		req.check_session = function() {
			if (self.sim_bad_session === false) {
				return true;
			}

			if (self.counter >= 10) {
				self.counter = 0;
				return false;
			}

			++ self.counter;
			return true;
		};

		next();
	});
	
	/* Gives access to the database object */
	this.app.use(function(req, res, next) {
		req.db = db;
		next();
	});

	this.app.use(this.app.router);

	/* Load routes */
	require('./routes')(this.app);

	/* Management of undefined routes */
	this.app.all('*', function(req, res) {
		res.send(404);
	});	
}

/**
 * Starts th HTTP server, which listens for
 * incoming requests
 */
Server.prototype.start = function() {
	this.app.listen(config.server.port);
	winston.info('server started');
};

/**
 * Enables/disabled the bad sessions in the
 * session simulation middleware
 * @param simulate True if the middleware may notify bad sessions, false otherwise
 */
Server.prototype.simulate_bad_session = function(simulate) {
	this.sim_bad_session = simulate;
};

module.exports = Server;
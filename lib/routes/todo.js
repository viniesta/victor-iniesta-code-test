/**
 * Manages the HTTP requests under the /todo path
 */
var winston = require('winston');

module.exports = function(app) {
	/**
	 * All the requests under the todo path must
	 * provide of session data: userId and sessionId
	 * Also, the session will be checked to be valid.
	 * In case of error, a 401 response will be sent
	 */
	app.all('/todo*', function(req, res, next) {
		if (!req.check_session()) {
			return res.send(401, 'Bad user/session');
		}

		if (!req.get('userId') || !req.get('sessionId')) {
			return res.send(401, 'Missing authorization data');
		}

		next();		
	});

	/**
	 * Manages the POST HTTP requests
	 * It expects a JSON-formatted body with the TODO note 
	 * information (title, body), assigned to the user specified
	 * in the header data.
	 * If the information is correct, it will add the note
	 * data to the database
	 * @returns 200 in case of success, and a HTTP error response otherwise
	 */
	app.post('/todo', function(req, res) {		
		if (!req.body) {
			return res.send(400, 'Malformed request - Missing body');
		}
		
		if ((!req.body.title) || (!req.body.body)) {
			return res.send(400, 'Malformed request - Wrong body');	
		}

		req.db.add_note(
			req.get('userId'),
			req.body.title,
			req.body.body)
		.then(function() {
			return res.send(200, 'Note added');	
		},
		function() {
			return res.send(500, 'Could not add note');
		})
		.fail(function(err) {
			winston.error('Exception when adding note', err);
			return res.send(500, 'Internal error');
		});
	});

	/**
	 * Manages the PUT HTTP requests
	 * It expects a JSON-formatted body with the TODO note 
	 * information (title, body), assigned to the user specified
	 * in the header data.
	 * If the information is correct, the note data will be 
	 * updated in the database	 
	 * @param :id ID of the TODO note to be udpated.
	 * @returns 200 in case of success, and a HTTP error response otherwise	 
	 */
	app.put('/todo/:id', function(req, res) {
		if (!req.body) {
			return res.send(400, 'Malformed request');
		}

		if ((!req.body.title) || (!req.body.body)) {
			return res.send(400, 'Malformed request - Wrong body');	
		}
	
		req.db.update_note(
			req.get('userId'), 
			req.params.id, 
			req.body.title, 
			req.body.body)
		.then(function(updated) {
			if (updated) {
				return res.send(200, 'Note updated');
			}

			return res.send(404, 'Note not found');
		},
		function() {
			return res.send(500, 'Could not update note');
		})
		.fail(function(err) {
			winston.error('Exception when updating note', err);
			return res.send(500, 'Internal error');
		});
	});

	/**
	 * Manages the DELETE HTTP requests
	 * Deletes a TODO note for the user specified
	 * in the headers
	 * @param :id ID of the TODO note to be deleted.
	 * @returns 200 in case of success, and a HTTP error response otherwise	 
	 */
	app.delete('/todo/:id', function(req, res) {
		req.db.remove_note(req.get('userId'),
							req.params.id)
		.then(function(note) {
			if (note) {
				return res.send(200, 'Note removed');
			}

			return res.send(404, 'Note not found');
		},
		function() {
			return res.send(500, 'Could not remove note');
		})
		.fail(function(err) {
			winston.error('Exception when removing note', err);
			return res.send(500, 'Internal error');
		});
	});	
};

/**
 * Manages the HTTP requests under the /user path
 */

var winston = require('winston');
module.exports = function(app) {

	/**
	 * Manages the GET HTTP requests
	 * Retrieves all the TODO notes for a given user
	 * in the headers
	 * @param :id ID of the user
	 * @returns 200 in case of success, with the array 
	 * of notes in the request body, and a HTTP error 
	 * response otherwise	 
	 */	
	app.get('/user/:id', function(req, res) {	
		req.db.get_user_notes(req.params.id)
		.then(function(notes) {
			return res.send(200, JSON.stringify(notes));
		},
		function() {
			return res.send(500, 'Error retrieving user notes');
		})
		.fail(function(err) {
			winston.error('Exception when removing note', err);
			return res.send(500, 'Internal error');
		});
	});
};

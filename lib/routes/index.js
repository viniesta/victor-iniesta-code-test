module.exports = function(app) {
	require('./todo')(app);
	require('./user')(app);
};
/**
 * Provides database access and management
 * of TODO notes for different users
 */

var Q = require('q');
var winston = require('winston');
var mongoose = require('mongoose');
var config = require('../config');

var noteSchema = mongoose.Schema({
    owner: String, /* user that created the note */
    title: String, /* title of the note */
    body: String /* body of the note */
});

/**
 * Prints a note document data
 */
noteSchema.methods.print = function() {
	winston.info(
		'TODO note data:\r\n' + 
		'\tOwner: ' + this.owner + '\r\n' +
		'\tTitle: ' + this.title + '\r\n' +
		'\tBody: ' + this.body
	);
};

var TODO = mongoose.model('TODO', noteSchema);

/**
 * Database class constructor
 * Initializes the database internal structures 
 * and objects
 */
function Database() {
	this.db = mongoose.connection;
}

/**
 * Connects to the database specified in the 
 * config.js file
 * @return Promise object that handles the connection result
 *		resolve: Returns nothing
 *		reject: Returns Error object
 */
Database.prototype.connect = function() {
	var deferred = Q.defer();
	var url = 'mongodb://' + config.database.host + ':' +
				config.database.port + '/' + 
				config.database.database;
	this.db.on('error', function database_error(err) {
		winston.error('Error in database', err);
		return deferred.reject(err);
	});

	this.db.once('open', function callback() {
		winston.info('Successfully connected to database');
		return deferred.resolve();
	});

	mongoose.connect(url);
	return deferred.promise;
};

/**
 * Adds a new TODO note to the database, associated to a given user
 * @param user ID of the note owner
 * @param title Title of the TODO note
 * @param body Body of the TODO note
 * @return Promise object that handles the note creation result
 *		resolve: Returns new note object
 *		reject: Returns Error object 
 */
Database.prototype.add_note = function(user, title, body) {
	winston.info('Adding new note to TODO list of user ' + user);
	var deferred = Q.defer();
	var note = new TODO({
		owner : user, 
		title : title, 
		body : body
	});

	note.save(function save_note(err, note) {
		if (err) {
			winston.error('Error saving document to database', err);
			return deferred.reject(err);
		}

		winston.info('Note successfully saved');
		note.print();
		return deferred.resolve(note);
	});

	return deferred.promise;
};

/**
 * Updates the data of an existing TODO note
 * @param user ID of the note owner
 * @param id ID of the TODO note
 * @param title New title of the TODO note
 * @param body New title of the TODO note
 * @return Promise object that handles the note update result
  *		resolve: Returns number of updated documents
 *		reject: Returns Error object
 */
Database.prototype.update_note = function(user, id, title, body) {
	winston.info('Updating note with id:' + id);
	var deferred = Q.defer();
	TODO.update(
		{
			_id: id, 
			owner: user
		}, 
		{ $set: 
			{ 
				title: title,
				body: body
			}
		},
		function(err, updated_docs) {
			if (err) {
				winston.error('Error updating existing note', err);
				return deferred.reject(err);
			}

			if (updated_docs === 0) {
				winston.info('No note found for user with given id');
			} else  {
				winston.info('Note updated successfully');
			}

			return deferred.resolve(updated_docs !== 0);
		}
	);

	return deferred.promise;
};

/**
 * Removes an existing TODO note from database
 * @param user ID of the note owner
 * @param id ID of the TODO note
 * @return Promise object that handles the note deletion result
 *		resolve: Returns deleted note object
 *		reject: Returns Error object
 */
Database.prototype.remove_note = function(user, id) {
	winston.info('Deleting note with id: ' + id);
	var deferred = Q.defer();
	TODO.findOneAndRemove(
		{ 
			_id: id,
			owner: user
		}, 
		function(err, note) {
			if (err) {
				winston.error('Error removing note', err);
				return deferred.reject(err);
			}
			
			if (note) {
				winston.info('Note removed successfully');
				note.print();
			} else {
				winston.info('No note to be removed');
			}

			return deferred.resolve(note);
		}
	);

	return deferred.promise;
};

/**
 * Retrieves all the TODO notes for a given user
 * @param user ID of the note owner
 * @return Promise object that handles the request result
 *		resolve: Returns array of user notes
 *		reject: Returns Error object 
 */
Database.prototype.get_user_notes = function(user) {
	winston.info('Searching notes for user: ' + user);
	var deferred = Q.defer();
	TODO.find(
		{
			owner : user
		},
		function(err, notes) {
			if (err) {
				winston.error('Error retrieving notes for user ', err);
				return deferred.reject(err);
			}

			var simple_notes = [];
			notes.forEach(function(note) {				
				simple_notes.push(
					{
						_id : note._id,
						owner : note.owner,
						title : note.title,
						body : note.body
					}
				);
			});

			return deferred.resolve(simple_notes);
		}
	);

	return deferred.promise;
};

/**
 * Deletes all the documents of the TODO collection
 * @return Promise object that handles the delete result
 *		resolve: Returns nothing
 *		reject: Returns Error object
 */
Database.prototype.delete_all_documents = function() {
	winston.info('Removing all documents from collection ' + config.database.database);
	var deferred = Q.defer();
	TODO.remove({}, function(err) {
		if (err) {
			return deferred.reject(err);
		}

		return deferred.resolve();
	});

	return deferred.promise;
};

module.exports = Database;
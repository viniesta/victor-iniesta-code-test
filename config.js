module.exports = {
	server : {
		port : 8080		
	},
	database : {
		host : "127.0.0.1",
		port : 27017,
		database : 'todo'
	}
};